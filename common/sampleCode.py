#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 14:30:23 2020

@author: ghauri
"""

#import requests
#page = requests.get("https")
#page
from moviepy.editor import VideoFileClip
import cv2
import pickle
import numpy as np
import glob

def getVideoSegments(video_clip,segLenth):
    segments = [(start, int(start+video_clip.fps*segLenth)) for start in range(0,int(video_clip.duration*video_clip.fps),int(video_clip.fps*segLenth))]
    return segments

def getSymmetricSegmetLength(video_clip,nSegments):
    print("duration of video: ",video_clip.duration)
    segLength=video_clip.duration//nSegments
    return segLength


#video_clip.duration
#video_clip.fps
#video_clip.get_frame(nthFrame)


baseVideo="C:\\Users\\ghaurij\\Downloads\\kaggleData\\dfdc_train_part_1\\*.mp4"
baseJson="C:\\Users\\ghaurij\\Downloads\\kaggleData\\dfdc_train_part_1\\*.json"

listvideos=glob.glob(baseVideo)
listJson=glob.glob(baseJson)

import json
#metaData=json.loads(listJson[0],encoding="ut8")

metaData=eval(open(listJson[0],"r").read())

video_dict = {}
segLenth=3
nSegments=100
nthFrame=50
impLevel=10

video_clip = VideoFileClip(listvideos[100])
video_clip.duration
video_clip.fps
frm=video_clip.get_frame(8)

frmAll=list(video_clip.iter_frames(with_times=False))

#allFrames=[x for x in frmAll]
#for x in frmAll:
#    print(x.shape)
#audioclip = AudioFileClip("some_video.avi")
#thumbnailBase="./static/videodata/thumbnails/"
#for vid in listvideos:
#    print("processing video: ",vid)
#    video_clip = VideoFileClip(vid)

cv2.imshow("12",frm)
cv2.waitKey(0)

#from views import *
#printMyName()


#pickle.dump(videoAnnotate,open("videoAnnotate.p","wb"));


#import zipfile
#
#base="C:\\Users\\ghaurij\\Downloads\\kaggleData\\"
#z = zipfile.ZipFile(base+"dfdc_train_part_01.zip", "r")
#
#for filename in z.namelist():
#    print('File:', filename)
#    bytes = z.read(filename)
#    print('has',len(bytes),'bytes')
	
	
#import face_recognition
#face_locations = face_recognition.face_locations(image)