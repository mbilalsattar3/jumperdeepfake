# coding: utf-8
import argparse
import time
import math
import os
import torch
import torch.nn as nn
import torch.onnx
from torch.autograd import Variable
import numpy as np
from skimage.transform import resize
from sklearn.model_selection import train_test_split
import numba
from numba import jit

from common import videoLoading as vl
import model

parser = argparse.ArgumentParser(description='PyTorch Wikitext-2 RNN/LSTM Language Model')
parser.add_argument('--model', type=str, default='ConvLSTM',
                    help='type of recurrent net (ConvLSTM)')
parser.add_argument('--in_dim', type=int, default=3,
                    help='input image channels')
parser.add_argument('--n_hid', type=int, default=[128, 64, 32],
                    help='number of hidden units per layer')
parser.add_argument('--n_layers', type=int, default=3,
                    help='number of layers')
parser.add_argument('--kernels', type=int, default=[(7, 7), (5, 5), (3, 3)],
                    help='number of layers')
parser.add_argument('--horizon', type=int, default=5,
                    help='number of layers')
parser.add_argument('--lr', type=float, default=20,
                    help='initial learning rate')
parser.add_argument('--clip', type=float, default=0.25,
                    help='gradient clipping')
parser.add_argument('--epochs', type=int, default=1,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=20,
                    help='batch size')
parser.add_argument('--dropout', type=float, default=0.2,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--log_interval', type=int, default=2, metavar='N',
                    help='report interval')
parser.add_argument('--save', type=str, default='model.pt',
                    help='path to save the final model')
parser.add_argument('--onnx-export', type=str, default='',
                    help='path to export the final model in onnx format')

args = parser.parse_args()


def repackage_hidden(h):
    """Wraps hidden states in new Variables, to detach them from their history."""
    if type(h) == Variable:
        return Variable(h.data)
    else:
        return tuple(repackage_hidden(v) for v in h)


# Set the random seed manually for reproducibility.
torch.manual_seed(args.seed)
if torch.cuda.is_available():
    if not args.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

device = torch.device("cuda" if args.cuda else "cpu")

###############################################################################
# Load data
###############################################################################

rootBase = "D:\\repos\\deepfdata\\*\\"

vidDir = vl.getVidDirs(rootBase)[0]
metaData = vl.getMetaData(vidDir)
trainDir = vl.get1DVidLst(metaData, balance=True)

vid_len_pixels = 1080 // 20
vid_width_pixels = 1920 // 20


# @jit(nopython=True, parallel=True)
def get_data(n_videos, start_index):
    data = []
    target = []

    for video_index in range(n_videos):
        video_name = trainDir[start_index + video_index][vl.configDict["vidName"]]
        label = trainDir[start_index + video_index][vl.configDict["label"]]
        frame_list = vl.getVidFrames(vidDir + video_name)

        for i in range(len(frame_list)):
            frame_list[i] = resize(frame_list[i], (vid_len_pixels, vid_width_pixels))

        for i in range(0, len(frame_list) - args.horizon):
            data.append(np.stack(frame_list[i:i + args.horizon], axis=0))
            target.append(label)

    data = np.stack(data, axis=0)
    target = np.array(target)
    print(data.shape)

    return data, target


def batchify(data, labels, bsz):
    # Work out how cleanly we can divide the dataset into bsz parts.
    nbatch = data.size(0) // bsz
    # Trim off any extra elements that wouldn't cleanly fit (remainders).
    data = data.narrow(0, 0, nbatch * bsz)
    labels = labels.narrow(0, 0, nbatch * bsz)
    # Evenly divide the data across the bsz batches.
    data = data.view(bsz, -1, args.horizon, vid_len_pixels, vid_width_pixels, 3).contiguous()
    labels = labels.view(bsz, -1).contiguous()

    return data.to(device), labels.to(device)


###############################################################################
# Build the model
###############################################################################


model = model.ConvLSTM((vid_len_pixels, vid_width_pixels), input_dim=args.in_dim, hidden_dim=args.n_hid,
                       kernel_size=args.kernels, num_layers=args.n_layers, fc_net_out=True,
                       fc_net_input_shape=args.horizon*args.n_hid[-1]*vid_len_pixels*vid_width_pixels,
                       batch_first=True, bias=True, return_all_layers=False)

# args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied).to(device)

criterion = nn.CrossEntropyLoss()


###############################################################################
# Training code
###############################################################################


def evaluate(data, targets):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    total_loss = 0.

    with torch.no_grad():
        for batch in range(data.size(0)):
            output = model(data[batch])

            total_loss += criterion(output, targets[batch]).item()
    return total_loss / (len(data) - 1)


def train(lr, best_val_loss):
    # Turn on training mode which enables dropout.
    model.train()
    total_loss = 0.
    start_time = time.time()
    n_videos_to_load = 1

    for video_no in range(0, len(trainDir), n_videos_to_load):
        data, target = get_data(n_videos_to_load, video_no)
        train_data, val_data, train_targets, val_targets = train_test_split(data, target, test_size=0.20,
                                                                            random_state=42, stratify=target)

        train_data = torch.from_numpy(train_data).type(torch.FloatTensor if not args.cuda else torch.cuda.FloatTensor)
        train_targets = torch.from_numpy(train_targets).type(torch.LongTensor if not args.cuda else torch.LongTensor)
        train_data, train_targets = batchify(train_data, train_targets, args.batch_size)
        train_data = train_data.permute(0, 1, 2, 5, 3, 4)

        val_data = torch.from_numpy(val_data).type(torch.FloatTensor if not args.cuda else torch.cuda.FloatTensor)
        val_targets = torch.from_numpy(val_targets).type(torch.LongTensor if not args.cuda else torch.LongTensor)
        val_data, val_targets = batchify(val_data, val_targets, args.batch_size)
        val_data = val_data.permute(0, 1, 2, 5, 3, 4)

        print("video number ", video_no)

        # for batch, i in enumerate(range(0, train_data.size(0) - 1, args.batch_size)):
        for batch in range(train_data.size(0)):
            print(train_data[batch].shape)
            # Starting each batch, we detach the hidden state from how it was previously produced.
            # If we didn't, the model would try backpropagating all the way to start of the dataset.
            model.zero_grad()

            output = model(train_data[batch])
            print(train_targets[batch])
            loss = criterion(output, train_targets[batch])
            loss.backward()

            # hidden = repackage_hidden(hidden)

            # `clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)
            for p in model.parameters():
                p.data.add_(-lr, p.grad.data)

            total_loss += loss.item()

            if batch % args.log_interval == 0 and batch > 0:
                cur_loss = total_loss / args.log_interval
                elapsed = time.time() - start_time
                print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | '
                      'loss {:5.2f} | ppl {:8.2f}'.format(
                    epoch, batch, train_data.size(0), lr,
                    elapsed * 1000 / args.log_interval, cur_loss, math.exp(cur_loss)))
                total_loss = 0
                start_time = time.time()

        val_loss = evaluate(val_data, val_targets)
        print('-' * 89)
        print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
                'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
                                           val_loss, math.exp(val_loss)))
        print('-' * 89)
        # Save the model if the validation loss is the best we've seen so far.
        if not best_val_loss or val_loss < best_val_loss:
            with open(args.save+"."+str(epoch), 'wb') as f:
                torch.save(model, f)
            best_val_loss = val_loss
        else:
            # Anneal the learning rate if no improvement has been seen in the validation dataset.
            lr /= 4.0

    return lr, best_val_loss


def export_onnx(path, batch_size, seq_len):
    print('The model is also exported in ONNX format at {}'.
          format(os.path.realpath(args.onnx_export)))
    model.eval()
    dummy_input = torch.LongTensor(seq_len * batch_size).zero_().view(-1, batch_size).to(device)
    hidden = model.init_hidden(batch_size)
    torch.onnx.export(model, (dummy_input, hidden), path)
    torch.onnx.export(model, (dummy_input, hidden), path)


lr = args.lr
best_val_loss = None

# At any point you can hit Ctrl + C to break out of training early.
try:
    for epoch in range(1, args.epochs + 1):
        epoch_start_time = time.time()
        lr, best_val_loss = train(lr, best_val_loss)


except KeyboardInterrupt:
    print('-' * 89)
    print('Exiting from training early')

# Load the best saved model.
with open(args.save, 'rb') as f:
    model = torch.load(f)
    # after load the rnn params are not a continuous chunk of memory
    # this makes them a continuous chunk, and will speed up forward pass
    # Currently, only rnn model supports flatten_parameters function.
    if args.model is 'ConvLSTM':
        model.rnn.flatten_parameters()

# Run on test data.
# test_loss = evaluate(test_data)
